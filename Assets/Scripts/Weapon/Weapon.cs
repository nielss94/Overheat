﻿using UnityEngine;

[System.Serializable]
public abstract class Weapon : ScriptableObject, IPickUp
{
    [SerializeField]
    private new string name;
    public string Name => name; 
    
    [SerializeField]
    private float heat;
    public float Heat => heat;
    
    [SerializeField]
    protected float attackRange;

    [SerializeField]
    private float attackCooldown;
    public float AttackCooldown => attackCooldown;

    [SerializeField]
    private bool needsTarget = false;
    public bool NeedsTarget => needsTarget;
    
    [SerializeField]
    protected Attack attack;

    public abstract void Attack(Transform target);
    public abstract void Attack(Vector3 target);

    public void Take(Player player)
    {
        player.SetWeapon(this);
    }
    
}
