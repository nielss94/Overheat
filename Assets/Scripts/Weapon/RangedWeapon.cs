﻿using UnityEngine;

[CreateAssetMenu(menuName = "Weapons/Ranged Weapon")]
public class RangedWeapon : Weapon
{
    [Header("Ranged weapon attributes")]
    public GameObject projectile;

    public override void Attack(Transform target)
    {
        attack.Perform(target, projectile);
    }

    public override void Attack(Vector3 target)
    {
        attack.Perform(target, projectile);
    }
}
