﻿using UnityEngine;

[System.Serializable]
public abstract class Attack : MonoBehaviour
{
    [SerializeField]
    private string name;
    public string Name { get => name; }

    public virtual void Perform(Transform target, GameObject projectile = null) { }
    public virtual void Perform(Vector3 position, GameObject projectile = null) { }
}