﻿using UnityEngine;

[CreateAssetMenu(menuName = "Weapons/Melee Weapon")]
public class MeleeWeapon : Weapon
{
    public override void Attack(Transform target)
    {
        attack.Perform(target);
    }

    public override void Attack(Vector3 target)
    {
        attack.Perform(target);
    }
}
