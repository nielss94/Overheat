﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PickUpItem : MonoBehaviour
{
    public IPickUp pickUp;

    [SerializeField]
    public List<IPickUp> possiblePickUps;

    private void Awake()
    {
    }

    private void Start()
    {
        pickUp = possiblePickUps[Random.Range(0, possiblePickUps.Count - 1)];
    }
}
