﻿public interface IPickUp
{
    void Take(Player player);
}